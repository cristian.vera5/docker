# Docker

## ¿Qué es?
Lanzado en el año 2013 por Solomon Hykes, es un proyecto de codigo abierto que facilita el deploy de aplicaciones dentro de contenedores. Actua de forma similar a las máquinas virtuales, eliminando la necesidad de administrar directamente el hardware. 
    
        - Video de presentación de Docker al mundo: 

            https://www.youtube.com/watch?v=3N3n9FzebAA

## ¿Cómo funciona?
- La tecnología docker usa el kernel de linux y sus funciones, como los grupos de control y los espacios de nombre, para dividir los procesos y ejecutarlos de manera independiente.
    Proporciona un modelo de implementacion basado en imágenes, que permite compartir una aplicación o un conjunto de sercicios con todas sus dependencias en varios entornos. Docker, tambien automatiza la implementación de las aplicaciones en el entorno de contenedores.

## Ventajas contenedores Docker

### Capas y control de versiones de imágenes

- Cada archivo Docker está compuesto por varias capas que conforman una sola imagen. Cuando se ejecuta una acción, como "ejecutar" o "copiar", la imagen cambia y se crea una capa nueva.
- Docker reutiliza las capas para agilizar el diseño de los contenedores nuevos. 

### Restauración
-Uno de los mayores beneficios, es la de tener la capacidad de restauración. Todas las imagenes cuentan con capas. Si algo no gusta de la imagen actual, se puede restaurar a la version anterior.



## Docker vs máquinas virtuales

#### Soporte del sistema operativo
-El soporte entre ambos, es muy diferente. En las VM, cada una tiene su sisema operativo, por encima del SO del host, lo que hace que las    máquinas virtuales sean pesadas. En los contenedores, se comparte el sistema operativo host. Lo que hace que sean mucho mas livianos

#### Portabilidad
-Los contenedores Docker son facilmente portables porque no tienen sistema operativo separados. Para migrar una máquina virtual a otro host, se requiere mucho mas tiempo, debido a su tamaño.

#### Rendimiento
 -Docker puede iniciarse muy rapido en comparación con las máquinas virtuales, debido a su peso
 -En docker, no es necesario asignar recursos de forma permanente, como si lo es en las máquinas virtuales


## Instalar Docker en linux

-1- checkear que no este instalado. Para eso, ejecutamos el comando "docker"
-2- Entrar al modo SU con sudo su-
-3- Ejecutar "apt install docker.io"
-4- Corroborar la version que esta instalada, ejecutando "docker --version"

## Para correr la imagen del contenedor 
 - ir a docker hub y buscar la imagen que se necesite (mysql, nginx, etc)
 - copiar la información que allí figura en la opcion "Copy and paste to pull this image" 
 - en el terminal, ingresar el comando copiado previamente (se puede tagear aplicando ":", si no se quiere la ultima version. De lo contrario, se instalará la ultima)
  - Ejecutando el comando "docker ps", se puede ver el estado de los contenedores.
  - Para ejecutar el contenedor, copiamos el ID del mismo, en la columna "Container ID" y ejecutamos "docker start <id_container>"
  

